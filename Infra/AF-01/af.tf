resource "azurerm_storage_account" "stga" {
  account_replication_type = "LRS"
  account_tier = "Standard"
  location = "${var.location}"
  name = "${var.stga_name}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
}

resource "azurerm_app_service_plan" "asp" {
  location = "${var.location}"
  name = "${var.asp_name}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  kind = "FunctionApp"
  sku {
    size = "Y1"
    tier = "Dynamic"
  }
}

resource "azurerm_function_app" "af" {
  app_service_plan_id = "${azurerm_app_service_plan.asp.id}"
  location = "${var.location}"
  name = "${var.af_name}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  storage_connection_string = "${azurerm_storage_account.stga.primary_connection_string}"
  version = "~2"
}